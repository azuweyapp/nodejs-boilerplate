module.exports = {
  testRegex: "(/tests/compiled/.*)\\.(js?)$",
  moduleFileExtensions: ["js", "jsx", "json", "node"]
};
# NodeJS boilerplate

NodeJS boilerplate with TypeORM, Passport and HTTP/2 Support written in Typescript.

## Getting started on SSH

### 1. step on SSH: Clone this repository

```sh
git clone git@bitbucket.org:azuweyapp/nodejs-boilerplate.git
```

### 2. Step: Set upstream on SSH

```sh
cd nodejs-boilerplate/
git remote add upstream git@bitbucket.org:azuweyapp/nodejs-boilerplate.git
```

## Getting started on HTTPS

### 1. step on HTTPS: Clone this repository

```sh
git clone https://bitbucket.org/azuweyapp/nodejs-boilerplate.git
```

### 2. Step: Set upstream on HTTPS

```sh
cd nodejs-boilerplate/
git remote add upstream https://bitbucket.org/azuweyapp/nodejs-boilerplate.git
```

## From now on, the following commands can be executed in order to bring the changes from the official repository to your repository

### 1. Step: Fetch updates

```sh
git fetch upstream
```

### Now, you have all the updates in upstream/#{branchName}. The stable version is in upstream/master

### 2. Step: Go to your local master branch (if you are not)

```sh
git checkout master
```

### 3. Step: Merge the changes from upstream/master into your local master branch. This brings your fork's master branch into sync with the upstream repository, without losing your local changes

```sh
git merge upstream/master
```

## Configure the project

### Fields

* **ORM** : This one is the all ORM configuartion, in default we use SQLite 3 database, you find more info right [here](https://github.com/typeorm/typeorm/blob/master/docs/using-ormconfig.md)

* **local** : This one is our authentication system configuration, you find more info right [here](https://github.com/jaredhanson/passport-local#parameters)

* **webserver** : This one is just contain our webserver's port

* **test** : This one is for the tests, so this is optional, for production and developemtn build, only required for test build.

* **sessionstore** : This one is our session storage settings, in default we use in-memory database, but you can find more options [here](https://github.com/adrai/sessionstore)

## Before you build the project, you need SSL key and SSL Cert

### If you not have SSL Key and Cert and you not have a public domain for this system

```sh
./genkey.sh
```

### If you not have SSL Key and Cert but you have a public domain for this system

[Generate a key with Let's Encrypt](https://letsencrypt.org/getting-started)

### If you have SSL Key and Cert

```sh
cp {originalKeyPath} ${PWD}/ssl.key
cp {originalCertPath} ${PWD}/ssl.crt
```

## Build project [DEVELOPMENT]

### 1. Step: Compile Typescript to Javascript in debug mode

```sh
npm run build
```

### 2. Step: Open a new terminal or a new command prompt and run the debug/index.js

```sh
npm run debug
```

## Build project [TEST]

### 1. Step: Host a selenium server for the test

### If you use docker, but you should because this is the recommend way

```sh
sudo docker run -d -p 4444:4444 --net=host -h 127.0.0.1 --shm-size=2g selenium/standalone-chrome:3.12.0-cobalt
```

### If you not use docker, you can find the jar file [here](http://selenium-release.storage.googleapis.com/index.html), and also you have to install the chrome driver

```sh
java -jar selenium-server-standalone-x.x.x.jar
```

### 2. Step: Open a new terminal or a new command prompt and run tests

```sh
npm test
```

## Build project [PRODUCTION]

### 1. Step: Build docker image

```sh
docker build -t {yourProjectName}_image .
```

### 2. Step: Run docker image with exposed port

```sh
docker run -d -p 443:443 -p 80:80 --name {yourProjectName}_container {yourProjectName}_image
```

## Frequently Asked Questions

### [How to change User authentication](https://bitbucket.org/azuweyapp/nodejs-boilerplate/wiki/How-to-change-user-authentication)
FROM node:carbon

# Create app directory
WORKDIR /usr/src/app

# Copy the project file
COPY package*.json ./

RUN npm install

# Bundle app source
COPY . .

EXPOSE 80
EXPOSE 443

CMD [ "npm", "start" ]
import { Request, Response } from 'express';

/**
 * DELETE /logout request handler
 * 
 * @param request {Express.Request}
 * @param response {Express.Response}
 */
export const LogoutDelete = (
  request: Request,
  response: Response
) => {
  request.logout();
  response.redirect('/');
};
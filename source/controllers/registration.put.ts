import BCrypt from 'bcrypt';
import { Request, Response } from 'express';
import { Connection, getConnection, Repository } from 'typeorm';

import { User } from '../entities';

/**
 * PUT /registration request handler
 * 
 * @param request {Express.Request}
 * @param response {Express.Response}
 */
export const RegistrationPut = async (
  request: Request,
  response: Response
) => {
  // Get database connection what is 
  // already created in the index file
  const dbConnection: Connection =
    getConnection('serverConnection');

  // Connect to the database if disconnected
  if (!dbConnection.isConnected)
    await dbConnection.connect();

  // Create an instance of User repository
  // from database
  const userRepository: Repository<User> =
    dbConnection.getRepository(User);

  // Get the username from the request
  const username: string = request.query.regUsername ||
    request.params.regUsername || request.body.regUsername;

  // Create an instance of User from database
  let user: User = await userRepository.findOne({
    username
  });

  // If user is exist, then display
  // an error to the user
  if (user)
    response.render('registration', {
      message: 'This username is already be claimed!'
    });
  else {
    // Get the password from the request
    const password: string = request.query.regPassword ||
      request.params.regPassword || request.body.regPassword;

    // Create a new password hash
    // from the original password with bcrypt
    const passwordHash: string = BCrypt.hashSync(password, 10);

    // Get the displayName from the request
    const displayName: string = request.query.regDisplayName ||
      request.params.regDisplayName || request.body.regDisplayName;

    // Create an instance of User.
    user = new User();

    // Setup our user properties
    user.username = username;
    user.password = passwordHash;
    user.displayName = displayName;

    // Try to save our user to the database
    // through our user repository,
    // if it failed then display
    // an error to the user
    userRepository.save(user).then(
      user => request.login(user, (err) => {
        if (!err)
          response.redirect('profile');
        else
          response.render('registration', {
            message: err
          });
      })
    ).catch(reason => response.render('registration', {
      message: reason
    }));
  }
};
import { Request, Response } from 'express';

/**
 * GET /registration request handler
 * 
 * @param request {Express.Request}
 * @param response {Express.Response}
 */
export const RegistrationGet = (
  request: Request,
  response: Response
) => {
  response.render('registration', {
    message: request.flash('error')
  });
};
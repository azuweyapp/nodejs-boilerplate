import { Request, Response } from 'express';

/**
 * GET /profile request handler
 * 
 * @param request {Express.Request}
 * @param response {Express.Response}
 */
export const ProfileGet = (
  request: Request,
  response: Response
) => {
  response.render('profile', {
    message: request.flash('error')
  });
};
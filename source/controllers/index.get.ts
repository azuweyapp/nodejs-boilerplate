import { Request, Response } from 'express';

/**
 * GET / request handler
 * 
 * @param request {Express.Request}
 * @param response {Express.Response}
 */
export const IndexGet = (
  request: Request,
  response: Response
) => {
  response.render('index', {
    message: request.flash('error')
  });
};
import { NextFunction, Request, Response } from 'express';

/**
 * Authenticator middleware for view
 * 
 * @param request {Express.Request}
 * @param response {Express.Response}
 * @param next {Express.NextFunction}
 */
export const IsAuthenticated = (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  response.locals.isAuthenticated = request.isAuthenticated();
  if (response.locals.isAuthenticated)
    response.locals.displayname = request.user.displayName;
  next();
};